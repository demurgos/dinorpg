'use strict';

exports.dinozRace = {
    hippoclamp: 'hippoclamp',
    rocky: 'rocky',
    pteroz: 'pteroz',
    winks: 'winks',
    sirain: 'sirain',
    castivore: 'castivore',
    nuagoz: 'nuagoz',
    gorilloz: 'gorilloz',
    wanwan: 'wanwan',
    pigmou: 'pigmou',
    planaille: 'planaille',
    moueffe: 'moueffe'
};

exports.reward = {
    tropheeRocky: 'tropheeRocky',
    tropheePteroz: 'tropheePteroz',
    tropheeHippoclamp: 'tropheeHippoclam'
}

exports.nbrDinozInDinozShop = 15;