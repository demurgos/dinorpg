'use strict';

const db = require("../models");
const Player = db.player;
const EpicReward = db.epicReward;

module.exports = {
    getRewardFromArray: (playerId, rewardArray) => {
        return Player.findOne({
            attributes: [],
            include: {
                model: EpicReward,
                attributes: ['name'],
                where : { name: rewardArray },
                through: {
                    attributes: []
                },
                as: 'reward',
                required: false
            },
            where: { playerId: playerId }
        });
    },

    getMoney: (playerId) => {
        return Player.findOne({
            attributes: ['playerId', 'money'],
            where: { playerId: playerId }
        });
    },

    setPlayerMoney: (player) => {
        return player.save({
            fields: ['money'],
            where: { playerId: player.playerId }
        })
    }
}