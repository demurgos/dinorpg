'use strict';

class Constants {

	getDinoSWF() {
		return 'http://localhost:8081/api/data/swf/dino.swf';
	}

	getSDinoSWF() {
		return 'http://localhost:8081/api/data/swf/sdino.swf';
	}

}

export default new Constants();
